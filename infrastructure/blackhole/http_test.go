package blackhole

import (
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHTTPClientLoadGraphHappyPath(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Header().Set("Content-Type", "application/json")
		rw.Write([]byte(`{"graph_id":"00000000-0000-0000-0000-000000000000","root":{"children":[{"description":"Handles staking and unstaking operations, verifying the operations\u0026#39; compliance with the participants\u0026#39; balances. This rule is crucial for maintaining the economic integrity and security of the network, ensuring that staking activities are conducted fairly and within the ecosystem\u0026#39;s economic policies.","functionName":"ValidateTx","graphID":"12ee4b39-bac4-4484-a306-26112f3fc647","icon":"coins","id":"d6f3cf84938fbe6f01492645546b9643","name":"12ee4b39-bac4-4484-a306-26112f3fc647#ValidateTx","parent":"6cabd64da56756ff81e5eeb628cdf375","path":"//root/12ee4b39-bac4-4484-a306-26112f3fc647#ValidateTx","ruleName":"staking","type":"pi.rules.active"},{"description":"Manages user profile transactions, validating the ownership and validity of the modifications made to the profile. This rule is key for the data integrity of user profiles and the customization of the Utopixia experience, ensuring that user information is accurately and securely managed.","functionName":"ValidateTx","graphID":"189948ec-5a01-4945-a3ec-cd7672d9018b","icon":"user-circle","id":"6fab4d98d965f73d38a0fad3d836b82f","name":"189948ec-5a01-4945-a3ec-cd7672d9018b#ValidateTx","parent":"6cabd64da56756ff81e5eeb628cdf375","path":"//root/189948ec-5a01-4945-a3ec-cd7672d9018b#ValidateTx","ruleName":"user_profile","type":"pi.rules.active"},{"description":"Manages role transactions within guilds, validating the allocation and revocation of roles within the Utopixia community. This rule is essential for the harmonious and secure functioning of decentralized communities, ensuring that roles are assigned and revoked in accordance with the community\u0026#39;s guidelines.","functionName":"ValidateTx","graphID":"df9d7ff4-d9a2-4448-8dab-68d557b40184","icon":"users","id":"337e0ff55b6498cf66ac3ccdb9a9980a","name":"df9d7ff4-d9a2-4448-8dab-68d557b40184#ValidateTx","parent":"6cabd64da56756ff81e5eeb628cdf375","path":"//root/df9d7ff4-d9a2-4448-8dab-68d557b40184#ValidateTx","ruleName":"guilds","type":"pi.rules.active"},{"description":"Verifies if the data field is prefixed by a known and non-empty data field to prevent users from submitting irrelevant or malicious information. This rule is essential for maintaining the structure and integrity of data transactions within Utopixia, ensuring that only relevant and authorized information is processed.","functionName":"ValidateTx","graphID":"0dd95ffe-0eed-4412-88df-420f0a4499fa","icon":"database","id":"c38896416f078675f1dd2fc3e0dc4cc2","name":"0dd95ffe-0eed-4412-88df-420f0a4499fa#ValidateTx","parent":"6cabd64da56756ff81e5eeb628cdf375","path":"//root/0dd95ffe-0eed-4412-88df-420f0a4499fa#ValidateTx","ruleName":"is_known_data_field","type":"pi.rules.active"},{"description":"Ensures that the entity attempting to snapshot a graph is indeed the owner of the graph. This rule is vital for maintaining the integrity and ownership rights of graph data, allowing for secure and authorized snapshots of graph states for reference or backup purposes.","functionName":"ValidateTx","graphID":"206cb295-3cfb-469e-9215-c892cb1f1272","icon":"camera","id":"3ce51425bd24465940b6471e8773f4a3","name":"206cb295-3cfb-469e-9215-c892cb1f1272#ValidateTx","parent":"6cabd64da56756ff81e5eeb628cdf375","path":"//root/206cb295-3cfb-469e-9215-c892cb1f1272#ValidateTx","ruleName":"snapshot","type":"pi.rules.active"},{"description":"Controls token exchange transactions, verifying the sender\u0026#39;s eligibility to perform the exchange and the availability of funds. This rule ensures smooth and secure transactions within the Utopixia ecosystem, facilitating a reliable and efficient token economy.","functionName":"ValidateTx","graphID":"37a38845-a7d5-4b7f-8eaa-149d12cb109f","icon":"exchange-alt","id":"c53cf7d0708b99b2079766e38f9de4ef","name":"37a38845-a7d5-4b7f-8eaa-149d12cb109f#ValidateTx","parent":"6cabd64da56756ff81e5eeb628cdf375","path":"//root/37a38845-a7d5-4b7f-8eaa-149d12cb109f#ValidateTx","ruleName":"token_exchange","type":"pi.rules.active"},{"description":"Validates transactions based on data prefix and graph ownership. Ensures that actions are authorized and appropriate according to the target graph\u0026#39;s structure. This rule is crucial for conducting transactions that alter the graph in any way, ensuring they comply with the governance and structure of Utopixia\u0026#39;s ecosystem.","functionName":"ValidateTx","graphID":"4bdcd8d3-b6f7-4cbc-9bc9-e9c7cf66ab48","icon":"fa-bolt","id":"ed28dc2783d684462f3c2146b72856b4","name":"4bdcd8d3-b6f7-4cbc-9bc9-e9c7cf66ab48#ValidateTx","parent":"6cabd64da56756ff81e5eeb628cdf375","path":"//root/4bdcd8d3-b6f7-4cbc-9bc9-e9c7cf66ab48#ValidateTx","ruleName":"action","type":"pi.rules.active"},{"description":"Validates quest creations by \"quest masters\". This includes checking the identity of the quest creator and the compliance of the quest data, ensuring that the proposed quests meet Utopixia\u0026#39;s criteria and objectives for engaging and rewarding user activities.","functionName":"ValidateTx","graphID":"e19cdcfc-e660-41dc-b68b-9c4dc6506ed8","icon":"scroll","id":"b17d300df54503092257fac6109940be","name":"e19cdcfc-e660-41dc-b68b-9c4dc6506ed8#ValidateTx","parent":"6cabd64da56756ff81e5eeb628cdf375","path":"//root/e19cdcfc-e660-41dc-b68b-9c4dc6506ed8#ValidateTx","ruleName":"quests","type":"pi.rules.active"},{"description":"Manages transactions related to domain management, ensuring that the sender is the owner of the domain or has the necessary rights to make changes. This rule is fundamental for the integrity of domain transactions, protecting the ownership and rights associated with digital assets within Utopixia.","functionName":"ValidateTx","graphID":"75b0a341-6890-4abd-ba7e-0225a7b7b45d","icon":"clipboard-list","id":"b120f681b4621f6db6b2f303d577d59e","name":"75b0a341-6890-4abd-ba7e-0225a7b7b45d#ValidateTx","parent":"6cabd64da56756ff81e5eeb628cdf375","path":"//root/75b0a341-6890-4abd-ba7e-0225a7b7b45d#ValidateTx","ruleName":"registrar","type":"pi.rules.active"}],"graphName":"Rules of Utopixia","id":"6cabd64da56756ff81e5eeb628cdf375","labels":"rules","name":"root","path":"//root"},"checksum":""}`))
	}))
	defer server.Close()

	client := NewHTTPClient(server.URL)
	graph, err := client.LoadGraph(uuid.New())

	assert.NoError(t, err, "Expect no error from LoadGraph")
	assert.NotNil(t, graph, "Expected graph should not be nil")
}

func TestHTTPClientLoadGraphServerError(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.WriteHeader(http.StatusInternalServerError)
	}))
	defer server.Close()

	client := NewHTTPClient(server.URL)
	graph, err := client.LoadGraph(uuid.New())

	assert.NotNil(t, err)
	assert.Nil(t, graph)
}

func TestHTTPClientLoadGraphInvalidJson(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Write([]byte(`invalid json`))
	}))
	defer server.Close()

	client := NewHTTPClient(server.URL)
	graph, err := client.LoadGraph(uuid.New())

	assert.NotNil(t, err)
	assert.Nil(t, graph)
}

func TestHTTPClientLoadGraphInvalidUrl(t *testing.T) {
	client := NewHTTPClient("invalid url")
	graph, err := client.LoadGraph(uuid.New())

	assert.Error(t, err)
	assert.Nil(t, graph)
}
