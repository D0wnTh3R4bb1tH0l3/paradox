package filestorage

import (
	"crypto/x509"
	"encoding/json"
	"errors"
	"os"
	"strings"

	"github.com/google/uuid"

	"utopixia.com/paradox/infrastructure/keystore"
	"utopixia.com/paradox/infrastructure/logging"
)

/*
 * This file is part of Utopixia.
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

var ErrKeyDoesNotExist = errors.New("key does not exist")

type Storage interface {
	StoreKey(keyID uuid.UUID, encodedKey []byte, owner string) error
	GetKey(keyID uuid.UUID) ([]byte, string, error)
	DeleteKey(keyID uuid.UUID) error
}

type Directory struct {
	rootDirectory string
}

type JSONFileContent struct {
	Version    int    `json:"version"`
	PEMContent []byte `json:"pem_content"`
	Owner      string `json:"owner"`
}

func NewFileStorage(rootDirectory string, store keystore.Store) (*Directory, error) {
	d := &Directory{
		rootDirectory: rootDirectory,
	}
	keys, err := d.loadKeys()
	if err != nil {
		logging.Info("Can't load keys from storage : %s", err)
		return nil, err
	}

	if len(keys) == 0 {
		logging.Info("No keys found in storage")
	} else {
		logging.Info("Loaded %d keys from storage", len(keys))
	}

	for keyID := range keys {
		fileContent := &JSONFileContent{}
		err = json.Unmarshal(keys[keyID], &fileContent)
		if err != nil {
			logging.Error("Can't unmarshal key : %s", err)
			continue
		}

		decodedKey, err := x509.ParsePKCS1PrivateKey(fileContent.PEMContent)
		if err != nil {
			logging.Error("can't parse key : %s", err)
			continue
		}

		err = store.PutKey(keyID, keystore.NewRSAKeyWrapper(decodedKey, fileContent.Owner))
		if err != nil {
			logging.Error("Can't generate key : %s", err)
		}
	}
	return d, nil
}

// initStorageIfNeeded check if root directory exists, of not create it
func (f *Directory) initStorageIfNeeded() error {
	// Check if the directory exists
	_, err := os.Stat(f.rootDirectory)
	if os.IsNotExist(err) {
		logging.Info("Create storage directory %s", f.rootDirectory)
		// Create the directory
		err = os.MkdirAll(f.rootDirectory, os.ModePerm)
		if err != nil {
			return err
		}
	}
	return nil
}

// loadKeys load all the keys from the storage
func (f *Directory) loadKeys() (map[uuid.UUID][]byte, error) {
	// Initialize the storage if needed
	err := f.initStorageIfNeeded()
	if err != nil {
		return nil, err
	}

	// Get all the files in the directory
	files, err := os.ReadDir(f.rootDirectory)
	if err != nil {
		return nil, err
	}

	// Load all the keys
	keys := make(map[uuid.UUID][]byte)
	for _, file := range files {
		// Open the file
		filePtr, err := os.Open(f.rootDirectory + "/" + file.Name())
		if err != nil {
			return nil, err
		}
		defer filePtr.Close()

		// Read the file content
		stat, err := filePtr.Stat()
		if err != nil {
			logging.Warn("Can't get file stat : %s", err)
			continue
		}

		encodedKey := make([]byte, stat.Size())
		_, err = filePtr.Read(encodedKey)
		if err != nil {
			logging.Warn("Can't read key from file : %s", err)
			continue
		}

		// Get the key ID from the file name
		graphID := strings.TrimSuffix(file.Name(), ".json")
		keyID, err := uuid.Parse(graphID)
		if err != nil {
			logging.Warn("Can't parse key ID from file name : %s", err)
			continue
		}

		keys[keyID] = encodedKey
	}

	return keys, nil

}

// StoreKey get the Encoded private Key and store it in a file as PEM
func (f *Directory) StoreKey(userID uuid.UUID, encodedKey []byte, owner string) error {
	// Initialize the storage if needed
	err := f.initStorageIfNeeded()
	if err != nil {
		return err
	}

	// Create the file
	file, err := os.Create(f.rootDirectory + "/" + userID.String() + ".json")
	if err != nil {
		return err
	}
	defer file.Close()

	// Create the JSON content
	jsonContent := &JSONFileContent{
		Version:    1,
		PEMContent: encodedKey,
		Owner:      owner,
	}

	// Marshal the JSON content
	encodedContent, err := json.Marshal(jsonContent)
	if err != nil {
		return err
	}

	// Write the encoded key to the file
	_, err = file.Write(encodedContent)
	if err != nil {
		return err
	}

	return nil
}

// GetKey get the Encoded private Key from a file
func (f *Directory) GetKey(keyID uuid.UUID) ([]byte, string, error) {
	// Open the file
	file, err := os.Open(f.rootDirectory + "/" + keyID.String() + ".json")
	if err != nil {
		if os.IsNotExist(err) {
			return nil, "", ErrKeyDoesNotExist
		}
		return nil, "", err
	}
	defer file.Close()

	// Read the file content
	stat, err := file.Stat()
	if err != nil {
		return nil, "", err
	}

	encodedContent := make([]byte, stat.Size())
	_, err = file.Read(encodedContent)
	if err != nil {
		return nil, "", err
	}

	// Unmarshal the JSON content
	jsonContent := &JSONFileContent{}
	err = json.Unmarshal(encodedContent, jsonContent)
	if err != nil {
		return nil, "", err
	}

	return jsonContent.PEMContent, jsonContent.Owner, nil
}

// DeleteKey delete the file containing the Encoded private Key
func (f *Directory) DeleteKey(userID uuid.UUID) error {
	// Open the file
	err := os.Remove(f.rootDirectory + "/" + userID.String() + ".json")
	if err != nil {
		if os.IsNotExist(err) {
			return ErrKeyDoesNotExist
		}
	}
	return nil
}
