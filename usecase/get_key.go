package usecase

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"crypto/x509"
	"errors"
	"github.com/google/uuid"
	"utopixia.com/paradox/infrastructure/filestorage"
	"utopixia.com/paradox/infrastructure/keystore"
	"utopixia.com/paradox/infrastructure/logging"
)

var ErrKeyNotFound = errors.New("key not found")

type GetKeyUseCase interface {
	GetKey(keyID uuid.UUID) (keystore.KeyWrapper, error)
}

type getKeyUseCase struct {
	store            keystore.Store
	storageDirectory filestorage.Storage
}

func newGetKeyUseCase(store keystore.Store, fileStorage filestorage.Storage) GetKeyUseCase {
	return &getKeyUseCase{store: store, storageDirectory: fileStorage}
}

func (g *getKeyUseCase) GetKey(keyID uuid.UUID) (keystore.KeyWrapper, error) {
	key, err := g.store.GetKey(keyID)
	if err != nil {
		if errors.Is(err, keystore.ErrKeyNotFound) {
			// Try to load the keys from the storage
			keys, owner, err := g.storageDirectory.GetKey(keyID)
			if err != nil {
				if errors.Is(err, filestorage.ErrKeyDoesNotExist) {
					return nil, ErrKeyNotFound
				}
				logging.Error("can't get key : %s", err)
			}

			if keys != nil {
				decodedKey, err := x509.ParsePKCS1PrivateKey(keys)
				if err != nil {
					logging.Error("can't parse key : %s", err)

					return nil, err
				}

				err = g.store.PutKey(keyID, keystore.NewRSAKeyWrapper(decodedKey, owner))
				if err != nil {
					logging.Error("can't store key : %s", err)
					return nil, err
				}

				return keystore.NewRSAKeyWrapper(decodedKey, owner), nil
			} else {
				return nil, ErrKeyNotFound
			}
		}
	}
	return key, nil
}
