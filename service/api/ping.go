package api

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"net/http"
	"utopixia.com/paradox/domain"
)

const PingResponseMessage = "pong"

type PingResponse struct {
	IP      string `json:"ip"`
	Message string `json:"message"`
}

func NewPingHandler(wallet *domain.Wallet) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		NewSignedResponseWriter(wallet, w).SendSignedJSON(http.StatusOK, JSONResponse{
			Success: true,
			Message: PingResponse{
				IP:      r.RemoteAddr,
				Message: PingResponseMessage,
			},
		})
	}
}
