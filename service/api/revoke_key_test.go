package api

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"encoding/json"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"testing"
	"utopixia.com/paradox/domain"
	"utopixia.com/paradox/infrastructure/keystore"
	"utopixia.com/paradox/usecase"
)

func TestRevokeKeyHandlerHappyPath(t *testing.T) {
	mockUseCases := usecase.NewMockRevokeKeyUseCases(t)
	wallet := domain.NewWallet()
	keyID := uuid.New()

	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	require.NoError(t, err)

	keyWrapper := keystore.NewRSAKeyWrapper(privateKey, wallet.BlockchainAddress())
	mockUseCases.On("RevokeKey", keyID, wallet.BlockchainAddress()).Return(keyWrapper, nil)

	handler := NewRevokeKeyHandler(wallet, usecase.UseCases{RevokeKeyUseCase: mockUseCases})

	request := domain.KeyAccessRequest{
		KeyID:  keyID.String(),
		Action: "revoke_key",
	}
	serializedAccessRequest, _ := json.Marshal(request)
	tx := domain.NewTransaction(wallet.PublicKey(), uuid.New(), wallet.BlockchainAddress(), wallet.BlockchainAddress(), 0, string(serializedAccessRequest))

	requestTxBytes, _ := json.Marshal(tx.Sign(wallet.PrivateKey()))

	req, _ := http.NewRequest("POST", "/get_key", bytes.NewBuffer(requestTxBytes))
	rr := httptest.NewRecorder()

	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	mockUseCases.AssertExpectations(t)
}

func TestRevokeKeyHandlerInvalidAction(t *testing.T) {
	mockUseCases := usecase.NewMockRevokeKeyUseCases(t)
	wallet := domain.NewWallet()
	keyID := uuid.New()
	handler := NewRevokeKeyHandler(wallet, usecase.UseCases{RevokeKeyUseCase: mockUseCases})

	request := domain.KeyAccessRequest{
		KeyID:  keyID.String(),
		Action: "invalid_action",
	}
	serializedAccessRequest, _ := json.Marshal(request)
	tx := domain.NewTransaction(wallet.PublicKey(), uuid.New(), wallet.BlockchainAddress(), wallet.BlockchainAddress(), 0, string(serializedAccessRequest))

	requestTxBytes, _ := json.Marshal(tx.Sign(wallet.PrivateKey()))

	req, _ := http.NewRequest("POST", "/get_key", bytes.NewBuffer(requestTxBytes))
	rr := httptest.NewRecorder()

	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	mockUseCases.AssertExpectations(t)
}

func TestRevokeKeyHandlerUseCaseError(t *testing.T) {
	mockUseCases := usecase.NewMockRevokeKeyUseCases(t)
	wallet := domain.NewWallet()
	keyID := uuid.New()

	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	require.NoError(t, err)

	keyWrapper := keystore.NewRSAKeyWrapper(privateKey, wallet.BlockchainAddress())
	mockUseCases.On("RevokeKey", keyID, wallet.BlockchainAddress()).Return(keyWrapper, assert.AnError)

	handler := NewRevokeKeyHandler(wallet, usecase.UseCases{RevokeKeyUseCase: mockUseCases})

	request := domain.KeyAccessRequest{
		KeyID:  keyID.String(),
		Action: "revoke_key",
	}
	serializedAccessRequest, _ := json.Marshal(request)
	tx := domain.NewTransaction(wallet.PublicKey(), uuid.New(), wallet.BlockchainAddress(), wallet.BlockchainAddress(), 0, string(serializedAccessRequest))

	requestTxBytes, _ := json.Marshal(tx.Sign(wallet.PrivateKey()))

	req, _ := http.NewRequest("POST", "/get_key", bytes.NewBuffer(requestTxBytes))
	rr := httptest.NewRecorder()

	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	mockUseCases.AssertExpectations(t)
}

func TestRevokeKeyHandlerInvalidOwnerError(t *testing.T) {
	mockUseCases := usecase.NewMockRevokeKeyUseCases(t)
	wallet := domain.NewWallet()
	keyID := uuid.New()

	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	require.NoError(t, err)

	keyWrapper := keystore.NewRSAKeyWrapper(privateKey, "owner")
	mockUseCases.On("RevokeKey", keyID, wallet.BlockchainAddress()).Return(keyWrapper, assert.AnError)

	handler := NewRevokeKeyHandler(wallet, usecase.UseCases{RevokeKeyUseCase: mockUseCases})

	request := domain.KeyAccessRequest{
		KeyID:  keyID.String(),
		Action: "revoke_key",
	}
	serializedAccessRequest, _ := json.Marshal(request)
	tx := domain.NewTransaction(wallet.PublicKey(), uuid.New(), wallet.BlockchainAddress(), wallet.BlockchainAddress(), 0, string(serializedAccessRequest))

	requestTxBytes, _ := json.Marshal(tx.Sign(wallet.PrivateKey()))

	req, _ := http.NewRequest("POST", "/get_key", bytes.NewBuffer(requestTxBytes))
	rr := httptest.NewRecorder()

	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	mockUseCases.AssertExpectations(t)
}
