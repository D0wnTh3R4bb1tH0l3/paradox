package usecase

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"utopixia.com/paradox/domain"
	"utopixia.com/paradox/domain/graph"
	"utopixia.com/paradox/infrastructure/blackhole"
	"utopixia.com/paradox/infrastructure/keystore"
	"utopixia.com/paradox/infrastructure/logging"
)

// TargetElementConfig is a struct that holds the configuration for the target element
type TargetElementConfig struct {
	GraphID      uuid.UUID
	ElementID    string
	PropertyName string
}

type DecodeElementUseCase interface {
	DecodeElement(targetConfig TargetElementConfig, request *domain.SignedTransaction) (string, error)
}

type decodeElementUseCase struct {
	store     keystore.Store
	blackHole blackhole.Store
}

func newDecodeElementUseCase(keyStore keystore.Store, blackHole blackhole.Store) DecodeElementUseCase {
	return &decodeElementUseCase{store: keyStore, blackHole: blackHole}
}

func (g *decodeElementUseCase) DecodeElement(targetConfig TargetElementConfig, request *domain.SignedTransaction) (string, error) {
	targetGraph, err := g.blackHole.LoadGraph(targetConfig.GraphID)
	if err != nil {
		return "", err
	}

	element, err := targetGraph.Root.Find(targetConfig.ElementID)
	if err != nil {
		return "", err
	}

	accessControl, hasAccessControl := getAccessControl(element, targetConfig.PropertyName)
	if !hasAccessControl {
		return "", fmt.Errorf("access control not found")
	}

	if !accessControl.IsSatisfied(request) {
		return "", fmt.Errorf("access control not satisfied")
	}

	keyID, err := uuid.Parse(accessControl.KeyID)
	if err != nil {
		return "", err
	}

	err = checkKeyID(keyID, targetConfig.GraphID, targetConfig.ElementID)
	if err != nil {
		logging.Info("keyID doesn't match the expected value")
		return "", err

	}

	key, err := g.store.GetKey(keyID)
	if err != nil {
		logging.Info("can't get key: %s", err)
		return "", err
	}

	logging.Info("raw encrypted value: %s", accessControl.EncryptedValue)
	rawEncryptedValue, err := base64.StdEncoding.DecodeString(accessControl.EncryptedValue)
	if err != nil {
		return "", err
	}

	decodedValue, err := key.Decrypt(rawEncryptedValue)
	if err != nil {
		return "", err
	}
	logging.Info("decoded value: %s", string(decodedValue))
	return string(decodedValue), nil
}

func checkKeyID(keyID uuid.UUID, graphID uuid.UUID, elementID string) error {
	// keyID should be the same as the uuid v5 of the graphID and elementID
	expectedKeyID := uuid.NewSHA1(graphID, []byte(elementID))
	if keyID != expectedKeyID {
		return fmt.Errorf("keyID doesn't match the expected value")
	}

	return nil
}

func getAccessControl(element *graph.Element, propertyName string) (EncryptedProperty, bool) {
	accessControlProperty := fmt.Sprintf("%s.encrypted", propertyName)
	if !element.Has(accessControlProperty) {
		return EncryptedProperty{}, false
	}

	accessControl := EncryptedProperty{}

	rawProperty := element.Get(accessControlProperty)
	err := json.Unmarshal([]byte(rawProperty), &accessControl)
	if err != nil {
		logging.Error("can't unmarshal access control property: %s", err)
		return EncryptedProperty{}, false
	}

	return accessControl, true
}

// Const list of access modes
const (
	OwnerAccessMode       = "owner"
	AddressListAccessMode = "address_list"
)

// EncryptedProperty is a struct that holds the configuration for the encrypted property
type EncryptedProperty struct {
	KeyID               string   `json:"key_id"`
	EncryptedValue      string   `json:"encrypted_value"`
	AccessMode          string   `json:"access_mode"`
	AuthorizedAddresses []string `json:"authorized_addresses,omitempty"`
}

func (c EncryptedProperty) IsSatisfied(userRequest *domain.SignedTransaction) bool {
	if c.AccessMode == OwnerAccessMode {
		return true
	}

	if c.AccessMode == AddressListAccessMode {
		for _, authorizedAddress := range c.AuthorizedAddresses {
			if authorizedAddress == userRequest.RecipientBlockchainAddress {

				return true
			}
		}
		return false
	}

	return false
}
