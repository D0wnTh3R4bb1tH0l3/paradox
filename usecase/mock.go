package usecase

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"
	"testing"
	"utopixia.com/paradox/domain"
	"utopixia.com/paradox/infrastructure/keystore"
)

type GenerateKeyUseCaseMock struct {
	mock.Mock
}

func NewGenerateMock(t *testing.T) *GenerateKeyUseCaseMock {
	m := new(GenerateKeyUseCaseMock)
	m.Test(t)
	return m
}

func (m *GenerateKeyUseCaseMock) GenerateKey(keyID uuid.UUID, ownerAddress string) error {
	args := m.Called(keyID, ownerAddress)
	return args.Error(0)
}

type MockGetKeyUseCases struct {
	mock.Mock
}

func NewMockGetKeyUseCases(t *testing.T) *MockGetKeyUseCases {
	m := MockGetKeyUseCases{}
	m.Test(t)
	return &m
}

func (m *MockGetKeyUseCases) GetKey(keyID uuid.UUID) (keystore.KeyWrapper, error) {
	args := m.Called(keyID)
	return args.Get(0).(keystore.KeyWrapper), args.Error(1)
}

type MockRevokeKeyUseCases struct {
	mock.Mock
}

func NewMockRevokeKeyUseCases(t *testing.T) *MockRevokeKeyUseCases {
	m := MockRevokeKeyUseCases{}
	m.Test(t)
	return &m
}

func (m *MockRevokeKeyUseCases) RevokeKey(keyID uuid.UUID, requestSenderAddress string) error {
	args := m.Called(keyID, requestSenderAddress)
	return args.Error(1)
}

type MockDecodeElementUseCases struct {
	mock.Mock
}

func NewMockDecodeElementUseCases(t *testing.T) *MockDecodeElementUseCases {
	m := MockDecodeElementUseCases{}
	m.Test(t)
	return &m
}

func (m *MockDecodeElementUseCases) DecodeElement(targetConfig TargetElementConfig, request *domain.SignedTransaction) (string, error) {
	args := m.Called(targetConfig, request)
	return args.String(0), args.Error(1)
}
