package api

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"encoding/json"
	"github.com/google/uuid"
	"net/http"
	"utopixia.com/paradox/domain"
	"utopixia.com/paradox/usecase"
)

type ElementConfig struct {
	GraphID      string `json:"graph_id"`
	ElementID    string `json:"element_id"`
	PropertyName string `json:"property_name"`
	Action       string `json:"action"`
}

func NewDecodeElementHandler(wallet *domain.Wallet, uc usecase.UseCases) http.HandlerFunc {
	return WithCorsHeaders(func(w http.ResponseWriter, r *http.Request) {
		var signedTransaction domain.SignedTransaction
		// Parse the request
		err := json.NewDecoder(r.Body).Decode(&signedTransaction)
		if err != nil {
			NewSignedResponseWriter(wallet, w).SendSignedJSON(http.StatusBadRequest, JSONResponse{
				Success: false,
				Error:   err.Error(),
			})
			return
		}

		// Verify the transaction
		if err = signedTransaction.IsValid(); err != nil {
			NewSignedResponseWriter(wallet, w).SendSignedJSON(http.StatusBadRequest, JSONResponse{
				Success: false,
				Error:   err.Error(),
			})
			return
		}

		// Parse the request
		var request ElementConfig
		err = json.Unmarshal([]byte(signedTransaction.Data), &request)
		if err != nil {
			NewSignedResponseWriter(wallet, w).SendSignedJSON(http.StatusBadRequest, JSONResponse{
				Success: false,
				Error:   err.Error(),
			})
			return
		}

		// Verify action
		if request.Action != "decode_element" {
			NewSignedResponseWriter(wallet, w).SendSignedJSON(http.StatusBadRequest, JSONResponse{
				Success: false,
				Error:   "invalid action type",
			})
			return
		}

		targetGraphID, err := uuid.Parse(request.GraphID)
		if err != nil {
			NewSignedResponseWriter(wallet, w).SendSignedJSON(http.StatusBadRequest, JSONResponse{
				Success: false,
				Error:   err.Error(),
			})
			return
		}

		// Decode the element
		decodedElement, err := uc.DecodeElementUseCase.DecodeElement(usecase.TargetElementConfig{
			GraphID:      targetGraphID,
			ElementID:    request.ElementID,
			PropertyName: request.PropertyName,
		}, &signedTransaction)
		if err != nil {
			NewSignedResponseWriter(wallet, w).SendSignedJSON(http.StatusBadRequest, JSONResponse{
				Success: false,
				Error:   err.Error(),
			})
			return
		}

		// Send the response
		NewSignedResponseWriter(wallet, w).SendSignedJSON(http.StatusOK, JSONResponse{
			Success: true,
			Message: decodedElement,
		})
	})
}
