package domain

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import (
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/sha256"
	"encoding/json"
	"errors"
	"github.com/google/uuid"
	"utopixia.com/paradox/infrastructure/logging"
)

type KeyAccessRequest struct {
	KeyID  string `json:"key_id"`
	Action string `json:"action"`
}

type SignedTransaction struct {
	UUID                       uuid.UUID `json:"UUID"`
	SenderPublicKey            string    `json:"sender_public_key"`
	SenderBlockchainAddress    string    `json:"sender_blockchain_address"`
	RecipientBlockchainAddress string    `json:"recipient_blockchain_address"`
	Value                      int64     `json:"value"`
	Data                       string    `json:"data"`
	Signature                  string    `json:"signature"`
}

func (s *SignedTransaction) IsValid() error {
	if s.SenderBlockchainAddress == "" {
		logging.Error("Invalid transaction: sender blockchain address is empty")
		return errors.New("invalid transaction")
	}

	if s.RecipientBlockchainAddress == "" {
		logging.Error("Invalid transaction: recipient blockchain address is empty")
		return errors.New("invalid transaction")
	}

	if s.SenderPublicKey == "" {
		logging.Error("Invalid transaction: sender public key is empty")
		return errors.New("invalid transaction")
	}

	publicKey := PublicKeyFromString(s.SenderPublicKey)

	address := AddressFromPublicKey(publicKey)
	if address != s.SenderBlockchainAddress {
		logging.Error("Invalid transaction: sender address doesn't match sender public key")
		return errors.New("invalid transaction")
	}
	tx := NewTransaction(publicKey, s.UUID, s.SenderBlockchainAddress, s.RecipientBlockchainAddress, s.Value, s.Data)
	m, _ := tx.MarshalJSON()
	logging.Info("Transaction: %s", m)
	h := sha256.Sum256(m)

	signature := SignatureFromString(s.Signature)
	if !ecdsa.Verify(publicKey, h[:], signature.R, signature.S) {
		logging.Error("Invalid transaction: invalid signature")
		return errors.New("invalid transaction")
	}

	return nil
}

type Transaction struct {
	UUID                       uuid.UUID
	SenderPublicKey            *ecdsa.PublicKey
	SenderBlockchainAddress    string
	RecipientBlockchainAddress string
	Value                      int64
	Data                       string
}

func NewTransaction(publicKey *ecdsa.PublicKey, UUID uuid.UUID, sender string, recipient string, value int64, data string) *Transaction {
	return &Transaction{
		UUID:                       UUID,
		SenderPublicKey:            publicKey,
		SenderBlockchainAddress:    sender,
		RecipientBlockchainAddress: recipient,
		Data:                       data,
		Value:                      value,
	}
}

func (t *Transaction) Sign(privateKey *ecdsa.PrivateKey) *SignedTransaction {
	signature := t.GenerateSignature(privateKey)
	return &SignedTransaction{
		UUID:                       t.UUID,
		SenderPublicKey:            PublicKeyToString(t.SenderPublicKey),
		SenderBlockchainAddress:    t.SenderBlockchainAddress,
		RecipientBlockchainAddress: t.RecipientBlockchainAddress,
		Value:                      t.Value,
		Data:                       t.Data,
		Signature:                  signature.String(),
	}
}

func (t *Transaction) GenerateSignature(privateKey *ecdsa.PrivateKey) *Signature {
	m, _ := t.MarshalJSON()
	h := sha256.Sum256(m)
	r, s, _ := ecdsa.Sign(rand.Reader, privateKey, h[:])
	return &Signature{R: r, S: s}
}

func (t *Transaction) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		UUID      string `json:"UUID"`
		Sender    string `json:"sender_blockchain_address"`
		Recipient string `json:"recipient_blockchain_address"`
		Data      string `json:"data,omitempty"`
		Value     int64  `json:"value"`
	}{
		UUID:      t.UUID.String(),
		Sender:    t.SenderBlockchainAddress,
		Recipient: t.RecipientBlockchainAddress,
		Data:      t.Data,
		Value:     t.Value,
	})
}
