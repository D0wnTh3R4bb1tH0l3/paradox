package blackhole

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"
	"testing"
	"utopixia.com/paradox/domain/graph"
)

type StoreMock struct {
	mock.Mock
}

func NewStoreMock(t *testing.T) *StoreMock {
	m := &StoreMock{}
	m.Test(t)
	return m
}

func (m *StoreMock) LoadGraph(graphID uuid.UUID) (*graph.Graph, error) {
	args := m.Called(graphID)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*graph.Graph), args.Error(1)
}
