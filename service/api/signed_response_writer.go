package api

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"encoding/json"
	"net/http"
	"utopixia.com/paradox/domain"
	"utopixia.com/paradox/infrastructure/logging"
)

type JSONResponse struct {
	Success bool        `json:"success"`
	Error   string      `json:"error,omitempty"`
	Message interface{} `json:"message,omitempty"`
}

type SignedJSONResponse struct {
	Success   bool        `json:"success"`
	Error     string      `json:"error,omitempty"`
	Message   interface{} `json:"message,omitempty"`
	Signature string      `json:"signature"`
}

type SignedResponseWriter struct {
	Wallet      *domain.Wallet
	InnerWriter http.ResponseWriter
}

func NewSignedResponseWriter(wallet *domain.Wallet, innerWriter http.ResponseWriter) *SignedResponseWriter {
	return &SignedResponseWriter{
		Wallet:      wallet,
		InnerWriter: innerWriter,
	}
}

func (w *SignedResponseWriter) SendSignedJSON(status int, response JSONResponse) {
	if response.Success {
		response.Error = ""
	} else {
		response.Message = nil
	}

	marshalledResponse, err := json.Marshal(response)
	if err != nil {
		logging.Error("can't marshal response : %s", err)
		return
	}
	signature, err := w.Wallet.Sign(marshalledResponse)
	if err != nil {
		logging.Error("can't sign response : %s", err)
		return
	}

	signedResponse := SignedJSONResponse{
		Success:   response.Success,
		Message:   response.Message,
		Error:     response.Error,
		Signature: signature.String(),
	}

	SendJSON(w.InnerWriter, status, signedResponse)
}
