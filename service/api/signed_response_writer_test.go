package api

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"net/http"
	"testing"
	"utopixia.com/paradox/domain"
)

func TestSignedResponseWriter_SendSignedJSON(t *testing.T) {
	t.Run("send a success", func(t *testing.T) {
		mockedResponseWriter := MockedResponseWriter{}
		w := domain.NewWallet()
		response := JSONResponse{
			Success: true,
			Message: "A message",
		}

		responseWriter := NewSignedResponseWriter(w, &mockedResponseWriter)
		responseWriter.SendSignedJSON(http.StatusOK, response)

		signedResponse, err := mockedResponseWriter.DecodeSignedResponse()
		require.NoError(t, err)

		assert.Equal(t, response.Success, signedResponse.Success)
		assert.Equal(t, response.Message, signedResponse.Message)
		assert.Empty(t, signedResponse.Error)

		responseSignature := domain.SignatureFromString(signedResponse.Signature)
		serializedResponse, err := json.Marshal(response)
		require.NoError(t, err)
		assert.True(t, responseSignature.IsValidFor(w.PublicKey(), serializedResponse))

	})

	t.Run("send an error", func(t *testing.T) {
		mockedResponseWriter := MockedResponseWriter{}

		w := domain.NewWallet()

		responseWriter := NewSignedResponseWriter(w, &mockedResponseWriter)

		response := JSONResponse{
			Success: false,
			Error:   "An error",
		}

		responseWriter.SendSignedJSON(http.StatusOK, response)

		signedResponse, err := mockedResponseWriter.DecodeSignedResponse()
		require.NoError(t, err)

		assert.Equal(t, response.Success, signedResponse.Success)
		assert.Equal(t, response.Error, signedResponse.Error)
		assert.Empty(t, signedResponse.Message)

		responseSignature := domain.SignatureFromString(signedResponse.Signature)
		serializedResponse, err := json.Marshal(response)
		require.NoError(t, err)
		assert.True(t, responseSignature.IsValidFor(w.PublicKey(), serializedResponse))
	})
}

type MockedResponseWriter struct {
	Content string
}

func (m *MockedResponseWriter) Header() http.Header {
	return http.Header{}
}

func (m *MockedResponseWriter) Write(b []byte) (int, error) {
	m.Content = string(b)
	return len(b), nil
}

func (m *MockedResponseWriter) WriteHeader(_ int) {}

func (m *MockedResponseWriter) DecodeSignedResponse() (response SignedJSONResponse, err error) {
	err = json.Unmarshal([]byte(m.Content), &response)
	return response, err
}
