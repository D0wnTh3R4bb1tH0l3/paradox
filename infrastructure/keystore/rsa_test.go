package keystore_test

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"testing"
	"utopixia.com/paradox/infrastructure/keystore"

	"github.com/stretchr/testify/assert"
)

func TestRSAKeyWrapperEncryptionDecryption(t *testing.T) {
	privateKey, _ := rsa.GenerateKey(rand.Reader, 2048)
	wrapper := keystore.NewRSAKeyWrapper(privateKey, "owner")

	originalData := []byte("test data")
	encryptedData, err := wrapper.Encrypt(originalData)
	assert.Nil(t, err)
	assert.NotEqual(t, originalData, encryptedData)

	decryptedData, err := wrapper.Decrypt(encryptedData)
	assert.Nil(t, err)
	assert.Equal(t, originalData, decryptedData)
}

func TestRSAKeyWrapperDecryptionWithWrongKey(t *testing.T) {
	privateKey1, _ := rsa.GenerateKey(rand.Reader, 2048)
	privateKey2, _ := rsa.GenerateKey(rand.Reader, 2048)
	wrapper1 := keystore.NewRSAKeyWrapper(privateKey1, "owner")
	wrapper2 := keystore.NewRSAKeyWrapper(privateKey2, "owner")

	originalData := []byte("test data")
	encryptedData, _ := wrapper1.Encrypt(originalData)

	decryptedData, err := wrapper2.Decrypt(encryptedData)
	assert.NotNil(t, err)
	assert.NotEqual(t, originalData, decryptedData)
}

func TestRSAKeyWrapperEncodeDecode(t *testing.T) {
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	wrapper := keystore.NewRSAKeyWrapper(privateKey, "owner")

	encoded, err := wrapper.Encode()
	assert.Nil(t, err)

	key, err := x509.ParsePKCS1PublicKey(encoded)
	assert.Nil(t, err)
	assert.Equal(t, &privateKey.PublicKey, key)
}
