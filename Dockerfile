# Utilisez une image de base avec Go préinstallé
FROM golang:latest

# Définit le répertoire de travail dans l'image
WORKDIR /app/

# Copie le dossier local "src" dans l'image
COPY . /app

# Exécute le script de construction et d'exécution du binaire
RUN cd /app && go mod tidy && go mod vendor && go test ./... && go build -o paradox

EXPOSE 80

CMD ["./paradox", "/app/wallet.webcode", "/app/data"]
