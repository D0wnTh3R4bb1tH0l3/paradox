package usecase

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"github.com/google/uuid"
	"utopixia.com/paradox/infrastructure/filestorage"
	"utopixia.com/paradox/infrastructure/keystore"
	"utopixia.com/paradox/infrastructure/logging"
)

type RevokeKeyUseCase interface {
	RevokeKey(keyID uuid.UUID, requestSenderAddress string) error
}

type revokeKeyUseCase struct {
	store            keystore.Store
	storageDirectory filestorage.Storage
}

func newRevokeKeyUseCase(store keystore.Store, fileStorage filestorage.Storage) RevokeKeyUseCase {
	return &revokeKeyUseCase{store: store, storageDirectory: fileStorage}
}

func (g *revokeKeyUseCase) RevokeKey(keyID uuid.UUID, requestSenderAddress string) error {
	err := g.store.RemoveKey(keyID, requestSenderAddress)
	if err != nil {
		return err
	}

	err = g.storageDirectory.DeleteKey(keyID)
	if err != nil {
		logging.Error("can't delete key : %s", err)
	}

	return nil
}
