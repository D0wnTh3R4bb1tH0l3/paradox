package blackhole

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"net/http"
	"utopixia.com/paradox/domain/graph"
)

type HTTPClient struct {
	client *http.Client
	url    string
}

func NewHTTPClient(url string) *HTTPClient {
	return &HTTPClient{
		client: &http.Client{},
		url:    url,
	}
}

type LoadGraphResponse struct {
	GraphID uuid.UUID `json:"graph_id"`
	Root    any       `json:"root"`
}

func (h *HTTPClient) LoadGraph(graphID uuid.UUID) (*graph.Graph, error) {
	baseUrl := fmt.Sprintf("%s/black-hole/api/v1/graph/%s", h.url, graphID.String())
	req, err := http.NewRequest("GET", baseUrl, nil)
	if err != nil {
		return nil, err
	}

	resp, err := h.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("error while loading graph: status code %d", resp.StatusCode)
	}

	var response LoadGraphResponse
	if err := json.NewDecoder(resp.Body).Decode(&response); err != nil {
		return nil, err
	}

	rootAsMap, ok := response.Root.(map[string]interface{})
	if !ok {
		return nil, fmt.Errorf("invalid root type")
	}

	root, err := graph.ElementFromMap(rootAsMap)
	if err != nil {
		return nil, err
	}

	return &graph.Graph{
		ID:   response.GraphID,
		Root: root,
	}, nil
}
