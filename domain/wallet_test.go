package domain_test

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"utopixia.com/paradox/domain"
)

func TestNewWalletCreation(t *testing.T) {
	wallet := domain.NewWallet()
	assert.NotNil(t, wallet)
	assert.NotNil(t, wallet.PrivateKey())
	assert.NotNil(t, wallet.PublicKey())
}

func TestNewWalletWithDomainCreation(t *testing.T) {
	wallet := domain.NewWalletWithDomain("test.com")
	assert.NotNil(t, wallet)
	assert.NotNil(t, wallet.PrivateKey())
	assert.NotNil(t, wallet.PublicKey())
	assert.Equal(t, "test.com", wallet.Domain())
}

func TestWalletEncryptionDecryption(t *testing.T) {
	wallet := domain.NewWallet()
	payload := wallet.Encrypt("test message")
	assert.NotNil(t, payload)
	decryptedMessage := payload.Decrypt(*wallet)
	assert.Equal(t, "test message", decryptedMessage)
}

func TestWalletSignature(t *testing.T) {
	wallet := domain.NewWallet()
	signature, err := wallet.Sign([]byte("test message"))
	assert.Nil(t, err)
	assert.NotNil(t, signature)
}
