package filestorage

import (
	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"
	"testing"
)

/*
 * This file is part of Utopixia.
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

type MockStorage struct {
	mock.Mock
}

func NewMockStorage(t *testing.T) *MockStorage {
	m := new(MockStorage)
	m.Test(t)
	return m
}

func (m *MockStorage) StoreKey(userID uuid.UUID, encodedKey []byte) error {
	args := m.Called(userID, encodedKey)
	return args.Error(0)
}

func (m *MockStorage) GetKey(userID uuid.UUID) ([]byte, error) {
	args := m.Called(userID)
	if args.Get(0) == nil {
		return nil, args.Error(1)

	}
	return args.Get(0).([]byte), args.Error(1)
}

func (m *MockStorage) DeleteKey(userID uuid.UUID) error {
	args := m.Called(userID)
	return args.Error(0)
}
