package usecase

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"utopixia.com/paradox/infrastructure/blackhole"
	"utopixia.com/paradox/infrastructure/filestorage"
	"utopixia.com/paradox/infrastructure/keystore"
)

type UseCases struct {
	GenerateKeyUseCase   GenerateKeyUseCase
	GetKeyUseCase        GetKeyUseCase
	RevokeKeyUseCase     RevokeKeyUseCase
	DecodeElementUseCase DecodeElementUseCase
}

func NewUseCases(store keystore.Store, blackHole blackhole.Store, fileStorage filestorage.Storage) UseCases {
	return UseCases{
		GenerateKeyUseCase:   newGenerateKeyUseCase(store, fileStorage),
		GetKeyUseCase:        newGetKeyUseCase(store, fileStorage),
		RevokeKeyUseCase:     newRevokeKeyUseCase(store, fileStorage),
		DecodeElementUseCase: newDecodeElementUseCase(store, blackHole),
	}
}
