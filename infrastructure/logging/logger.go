package logging

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"fmt"
	"log"
	"strings"
)

const (
	ERROR = iota
	WARN
	INFO
	DEBUG
)

var (
	StdLogger  *Logger
	levelStack []int
)

func init() {
	StdLogger = NewLogger()
	levelStack = []int{}
}

type Logger struct {
	logLevel int
	Prefix   string
}

func Error(format string, a ...any) {
	StdLogger.Error(format, a...)
}

func SetPrefix(prefix string) {
	StdLogger.Prefix = prefix
}

func (l *Logger) Error(format string, a ...any) {
	if l.logLevel >= ERROR {
		truePrefix := l.Prefix
		if truePrefix != "" {
			truePrefix = fmt.Sprintf("[%s] ", truePrefix)
		}
		log.Println(fmt.Sprintf("%s[ERROR] %s", truePrefix, fmt.Sprintf(strings.Trim(format, "\n"), a...)))
	}
}

func Warn(format string, a ...any) {
	StdLogger.Warn(format, a...)
}

func (l *Logger) Warn(format string, a ...any) {
	if l.logLevel >= WARN {
		truePrefix := l.Prefix
		if truePrefix != "" {
			truePrefix = fmt.Sprintf("[%s] ", truePrefix)
		}
		log.Println(fmt.Sprintf("%s[WARN] %s", truePrefix, fmt.Sprintf(strings.Trim(format, "\n"), a...)))
	}
}

func Info(format string, a ...any) {
	StdLogger.Info(format, a...)
}

func ClearLine(number int) {
	for i := 0; i < number; i++ {
		log.Print("\033[1A\033[K")
	}
}

func (l *Logger) Info(format string, a ...any) {
	if l.logLevel >= INFO {
		truePrefix := l.Prefix
		if truePrefix != "" {
			truePrefix = fmt.Sprintf("[%s] ", truePrefix)
		}

		log.Println(fmt.Sprintf("%s[INFO] %s", truePrefix, fmt.Sprintf(strings.Trim(format, "\n"), a...)))
	}
}

func Debug(format string, a ...any) {
	StdLogger.Debug(format, a...)
}

func (l *Logger) Debug(format string, a ...any) {
	if l.logLevel >= DEBUG {
		truePrefix := l.Prefix
		if truePrefix != "" {
			truePrefix = fmt.Sprintf("[%s] ", truePrefix)
		}

		log.Println(fmt.Sprintf("%s[DEBUG] %s", truePrefix, fmt.Sprintf(strings.Trim(format, "\n"), a...)))
	}
}

func NewLogger() *Logger {
	return &Logger{logLevel: INFO}
}

func PushLevel(logLevel int) {
	levelStack = append(levelStack, StdLogger.logLevel)
	StdLogger.logLevel = logLevel
}

func PopLevel() {
	if len(levelStack) == 0 {
		return
	}
	StdLogger.logLevel = levelStack[len(levelStack)-1]
	levelStack = levelStack[:len(levelStack)-1]
}
