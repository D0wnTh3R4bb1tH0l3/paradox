package keystore

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"crypto/rand"
	"crypto/rsa"
	"errors"
	"github.com/google/uuid"
)

var ErrKeyNotFound = errors.New("key not found")
var ErrKeyAlreadyExists = errors.New("key already exists")

// InMemoryStore is a simple in-memory keystore that provide RSA keys
type InMemoryStore struct {
	keys map[uuid.UUID]*RSAKeyWrapper
}

// NewInMemoryStore create a new InMemoryStore
func NewInMemoryStore() *InMemoryStore {
	return &InMemoryStore{
		keys: make(map[uuid.UUID]*RSAKeyWrapper),
	}
}

// GetKey get a key from the store
func (s *InMemoryStore) GetKey(keyID uuid.UUID) (KeyWrapper, error) {
	if _, exists := s.keys[keyID]; !exists {
		return nil, ErrKeyNotFound
	}

	if key, exists := s.keys[keyID]; exists {
		return key, nil
	}

	return nil, ErrKeyNotFound
}

// PutKey put a key in the store
func (s *InMemoryStore) PutKey(keyID uuid.UUID, key KeyWrapper) error {
	if _, exists := s.keys[keyID]; exists {
		return ErrKeyAlreadyExists
	}

	s.keys[keyID] = key.(*RSAKeyWrapper)
	return nil
}

// GenerateKey generate a key in the store
func (s *InMemoryStore) GenerateKey(keyID uuid.UUID, ownerAddress string) error {
	if _, exists := s.keys[keyID]; exists {
		return ErrKeyAlreadyExists
	}

	key, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return err
	}

	s.keys[keyID] = NewRSAKeyWrapper(key, ownerAddress)
	return nil
}

// RemoveKey remove a key from the store
func (s *InMemoryStore) RemoveKey(keyID uuid.UUID, requestSenderAddress string) error {
	if _, exists := s.keys[keyID]; !exists {
		return ErrKeyNotFound
	}

	if s.keys[keyID].Owner() != requestSenderAddress {
		return errors.New("only the owner can remove the key")
	}

	delete(s.keys, keyID)
	return nil
}
