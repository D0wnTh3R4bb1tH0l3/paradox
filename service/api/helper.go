package api

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"net/http"
	"utopixia.com/paradox/domain"
)

func BuildChiRouter(wallet *domain.Wallet) *chi.Mux {
	r := chi.NewRouter()
	r.NotFound(func(rw http.ResponseWriter, r *http.Request) {
		JsonError(rw, http.StatusNotFound, "Not found")
	})

	r.MethodNotAllowed(func(rw http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodOptions {
			JsonSuccess(rw, "ok")
			return
		}
		JsonError(rw, http.StatusNotFound, "Not found")
	})

	r.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			headers := rw.Header()
			headers.Set("Content-Type", "application/json; charset=utf-8")
			headers.Set("Access-Control-Allow-Origin", "*")
			headers.Set("Access-Control-Allow-Methods", "*")
			headers.Set("Access-Control-Allow-Headers", "*")
			next.ServeHTTP(rw, r)
		})
	})

	r.Get("/ping", NewPingHandler(wallet))
	r.Get("/public_key", func(rw http.ResponseWriter, r *http.Request) {
		NewSignedResponseWriter(wallet, rw).SendSignedJSON(http.StatusOK, JSONResponse{
			Success: true,
			Message: wallet.PublicKeyStr(),
		})
	})
	return r
}

func SendJSON(rw http.ResponseWriter, status int, data interface{}) {
	if status != http.StatusOK {
		rw.WriteHeader(status)
	}

	json.NewEncoder(rw).Encode(data) // nolint: errcheck
}

func JsonError(rw http.ResponseWriter, code int, errorMsg string) {
	SendJSON(rw, code, JSONResponse{
		Success: false,
		Error:   errorMsg,
	})
}

func JsonSuccess(rw http.ResponseWriter, message interface{}) {
	m, _ := json.Marshal(JSONResponse{
		Success: true,
		Message: message,
	})
	SendJSON(rw, 200, m)
}

func WithCorsHeaders(handler func(w http.ResponseWriter, req *http.Request)) func(w http.ResponseWriter, req *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Access-Control-Allow-Headers", "Authorization, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Access-Control-Allow-Methods", "*")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Max-Age", "3600")
		handler(w, req)
	}
}
