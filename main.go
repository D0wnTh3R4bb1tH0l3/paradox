package main

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"utopixia.com/paradox/domain"
	"utopixia.com/paradox/infrastructure/blackhole"
	"utopixia.com/paradox/infrastructure/filestorage"
	"utopixia.com/paradox/infrastructure/keystore"
	"utopixia.com/paradox/infrastructure/logging"
	"utopixia.com/paradox/service/api"
	"utopixia.com/paradox/usecase"
)

// getFileContent load a file content
func getFileContent(filePath string) ([]byte, error) {
	return ioutil.ReadFile(filePath)
}

func main() {
	logging.PushLevel(logging.DEBUG)
	logging.Info("Starting Paradox")
	// The first argument of the command line is the file path
	filePath := os.Args[1]
	// Load the file content
	fileContent, err := getFileContent(filePath)
	if err != nil {
		logging.Error("can't read file : %s", err)
		return
	}
	wallet, err := domain.FromWebcode(string(fileContent))
	if err != nil {
		logging.Error("can't create wallet : %s", err)
		return
	}

	storagePath := os.Args[2]
	inMemoryStore := keystore.NewInMemoryStore()
	fileStorage, err := filestorage.NewFileStorage(storagePath, inMemoryStore)
	if err != nil {
		logging.Error("can't create file storage : %s", err)
		return
	}

	logging.SetPrefix("paradox")
	blackHole := blackhole.NewHTTPClient("https://ororea.com")

	uc := usecase.NewUseCases(inMemoryStore, blackHole, fileStorage)
	server := &http.Server{
		Addr:    fmt.Sprintf(":%d", 80),
		Handler: api.NewRouter(wallet, uc),
	}
	if err := server.ListenAndServe(); err != nil {
		logging.Error("%s", err.Error())
	}
}
