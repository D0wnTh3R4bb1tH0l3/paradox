package api

import (
	"bytes"
	"encoding/json"
	"github.com/google/uuid"
	"net/http"
	"net/http/httptest"
	"testing"
	"utopixia.com/paradox/domain"
	"utopixia.com/paradox/usecase"

	"github.com/stretchr/testify/assert"
)

func TestGenerateKeyHandlerHappyPath(t *testing.T) {
	mockUseCase := usecase.NewGenerateMock(t)
	uc := usecase.UseCases{
		GenerateKeyUseCase: mockUseCase,
	}
	wallet := domain.NewWallet()
	keyID := uuid.New()
	mockUseCase.On("GenerateKey", keyID, wallet.BlockchainAddress()).Return(nil)

	handler := NewGenerateKeyHandler(wallet, uc)

	request := domain.KeyAccessRequest{
		KeyID:  keyID.String(),
		Action: "generate_key",
	}

	requestBytes, _ := json.Marshal(request)
	requestTx := domain.Transaction{
		UUID:                       uuid.New(),
		SenderPublicKey:            wallet.PublicKey(),
		SenderBlockchainAddress:    wallet.BlockchainAddress(),
		RecipientBlockchainAddress: wallet.BlockchainAddress(),
		Value:                      0,
		Data:                       string(requestBytes),
	}

	requestTxBytes, _ := json.Marshal(requestTx.Sign(wallet.PrivateKey()))
	req, _ := http.NewRequest("POST", "/generate_key", bytes.NewBuffer(requestTxBytes))
	rr := httptest.NewRecorder()

	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	mockUseCase.AssertExpectations(t)
}

func TestGenerateKeyHandlerInvalidAction(t *testing.T) {
	mockUseCase := usecase.NewGenerateMock(t)
	uc := usecase.UseCases{
		GenerateKeyUseCase: mockUseCase,
	}
	wallet := domain.NewWallet()

	handler := NewGenerateKeyHandler(wallet, uc)

	request := domain.KeyAccessRequest{
		KeyID:  uuid.NewString(),
		Action: "invalid_action",
	}

	requestBytes, _ := json.Marshal(request)
	requestTx := domain.Transaction{
		UUID:                       uuid.New(),
		SenderPublicKey:            wallet.PublicKey(),
		SenderBlockchainAddress:    wallet.BlockchainAddress(),
		RecipientBlockchainAddress: wallet.BlockchainAddress(),
		Value:                      0,
		Data:                       string(requestBytes),
	}

	requestTxBytes, _ := json.Marshal(requestTx.Sign(wallet.PrivateKey()))
	req, _ := http.NewRequest("POST", "/generate_key", bytes.NewBuffer(requestTxBytes))
	rr := httptest.NewRecorder()

	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	mockUseCase.AssertExpectations(t)
}

func TestGenerateKeyHandlerUseCaseError(t *testing.T) {
	mockUseCase := usecase.NewGenerateMock(t)
	uc := usecase.UseCases{
		GenerateKeyUseCase: mockUseCase,
	}
	wallet := domain.NewWallet()
	keyID := uuid.New()
	mockUseCase.On("GenerateKey", keyID, wallet.BlockchainAddress()).Return(assert.AnError)

	handler := NewGenerateKeyHandler(wallet, uc)

	request := domain.KeyAccessRequest{
		KeyID:  keyID.String(),
		Action: "generate_key",
	}

	requestBytes, _ := json.Marshal(request)
	requestTx := domain.Transaction{
		UUID:                       uuid.New(),
		SenderPublicKey:            wallet.PublicKey(),
		SenderBlockchainAddress:    wallet.BlockchainAddress(),
		RecipientBlockchainAddress: wallet.BlockchainAddress(),
		Value:                      0,
		Data:                       string(requestBytes),
	}

	requestTxBytes, _ := json.Marshal(requestTx.Sign(wallet.PrivateKey()))
	req, _ := http.NewRequest("POST", "/generate_key", bytes.NewBuffer(requestTxBytes))
	rr := httptest.NewRecorder()

	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	mockUseCase.AssertExpectations(t)
}
