package graph

import "testing"

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

func TestNewElementCreation(t *testing.T) {
	e := NewGraphElement("test")
	if e.Name != "test" {
		t.Errorf("Expected 'test', got %v", e.Name)
	}
}

func TestNewElementWithParentCreation(t *testing.T) {
	parent := NewGraphElement("parent")
	e := NewGraphElementWithParent(parent, "test")
	if e.Parent != parent {
		t.Errorf("Expected parent, got %v", e.Parent)
	}
}

func TestElementToMapConversion(t *testing.T) {
	e := NewGraphElement("test")
	m := e.ToMap()
	if m["name"] != "test" {
		t.Errorf("Expected 'test', got %v", m["name"])
	}
}

func TestElementHasParent(t *testing.T) {
	parent := NewGraphElement("parent")
	e := NewGraphElementWithParent(parent, "test")
	if !e.HasParent() {
		t.Errorf("Expected true, got false")
	}
}

func TestElementHasChildren(t *testing.T) {
	e := NewGraphElement("test")
	child := NewGraphElement("child")
	e.AddChildElement(child)
	if !e.HasChildren() {
		t.Errorf("Expected true, got false")
	}
}

func TestElementFind(t *testing.T) {
	e := NewGraphElement("test")
	child := NewGraphElement("child")
	e.AddChildElement(child)
	found, _ := e.Find(child.ID)
	if found != child {
		t.Errorf("Expected child, got %v", found)
	}
}

func TestElementSetGet(t *testing.T) {
	e := NewGraphElement("test")
	e.Set("property", "value")
	if e.Get("property") != "value" {
		t.Errorf("Expected 'value', got %v", e.Get("property"))
	}
}

func TestElementAddChild(t *testing.T) {
	e := NewGraphElement("test")
	child := e.AddChild("child")
	if child.Name != "child" {
		t.Errorf("Expected 'child', got %v", child.Name)
	}
}

func TestElementRemoveChild(t *testing.T) {
	e := NewGraphElement("test")
	child := e.AddChild("child")
	e.RemoveChild(child)
	if e.HasChildren() {
		t.Errorf("Expected false, got true")
	}
}
