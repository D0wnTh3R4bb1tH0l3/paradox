package usecase

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"crypto/rsa"
	"crypto/x509"
	"errors"
	"github.com/google/uuid"
	"utopixia.com/paradox/infrastructure/filestorage"
	"utopixia.com/paradox/infrastructure/keystore"
	"utopixia.com/paradox/infrastructure/logging"
)

var ErrKeyAlreadyExists = errors.New("key already exists")

type GenerateKeyUseCase interface {
	GenerateKey(keyID uuid.UUID, ownerAddress string) error
}

type generateKeyUseCase struct {
	store            keystore.Store
	storageDirectory filestorage.Storage
}

func newGenerateKeyUseCase(store keystore.Store, fileStorage filestorage.Storage) GenerateKeyUseCase {
	return &generateKeyUseCase{store: store, storageDirectory: fileStorage}
}

func (g *generateKeyUseCase) GenerateKey(keyID uuid.UUID, ownerAddress string) error {
	key, err := g.store.GetKey(keyID)
	if err == nil && key != nil {
		return ErrKeyAlreadyExists
	}

	err = g.store.GenerateKey(keyID, ownerAddress)
	if err != nil {
		return err
	}

	key, err = g.store.GetKey(keyID)
	if err != nil {
		return ErrKeyNotFound
	}

	encodedKey, err := key.Encode()
	err = g.storageDirectory.StoreKey(keyID, encodedKey, ownerAddress)
	if err != nil {
		logging.Error("can't store key : %s", err)
	}

	return err
}

func encodePrivateKeyToPEM(key *rsa.PrivateKey) []byte {
	return x509.MarshalPKCS1PrivateKey(key)
}
