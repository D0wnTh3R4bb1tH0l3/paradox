package usecase

import (
	"crypto/rand"
	"crypto/rsa"
	"encoding/base64"
	"fmt"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
	"utopixia.com/paradox/domain"
	"utopixia.com/paradox/domain/graph"
	"utopixia.com/paradox/infrastructure/blackhole"
	"utopixia.com/paradox/infrastructure/keystore"
)

func TestDecodeElementHappyPath(t *testing.T) {
	key1, err := rsa.GenerateKey(rand.Reader, 2048)
	require.NoError(t, err)

	targetGraphID := uuid.New()
	wrapper := keystore.NewRSAKeyWrapper(key1, "owner")

	keyID := uuid.NewSHA1(targetGraphID, []byte("element1"))
	// Setup
	store := keystore.NewStoreMock(t)
	store.On("GetKey", keyID).Return(wrapper, nil)

	decodedValue := "TestValue"
	encryptedProperty, err := wrapper.Encrypt([]byte(decodedValue))
	require.NoError(t, err)
	encodedValue := base64.StdEncoding.EncodeToString(encryptedProperty)

	blackHole := blackhole.NewStoreMock(t)
	blackHole.On("LoadGraph", targetGraphID).Return(&graph.Graph{
		Root: &graph.Element{
			ID: "element1",
			Properties: map[string]string{
				"property1.encrypted": fmt.Sprintf(`{"access_mode": "address_list", "key_id":"%s", "authorized_addresses": ["address1","address2"], "encrypted_value": "%s"}`, keyID, encodedValue),
			},
		},
	}, nil)

	useCase := newDecodeElementUseCase(store, blackHole)

	targetConfig := TargetElementConfig{
		GraphID:      targetGraphID,
		ElementID:    "element1",
		PropertyName: "property1",
	}
	request := &domain.SignedTransaction{
		SenderBlockchainAddress:    "owner",
		RecipientBlockchainAddress: "address2",
	}

	// Execute
	decoded, err := useCase.DecodeElement(targetConfig, request)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}

	assert.Equal(t, decodedValue, decoded)

	store.AssertExpectations(t)
	blackHole.AssertExpectations(t)
}

func TestDecodeElementWhenGraphLoadFails(t *testing.T) {
	// Setup
	store := keystore.NewStoreMock(t)
	blackHole := blackhole.NewStoreMock(t)
	targetGraphID := uuid.New()
	blackHole.On("LoadGraph", targetGraphID).Return(nil, fmt.Errorf("error"))
	useCase := newDecodeElementUseCase(store, blackHole)

	targetConfig := TargetElementConfig{
		GraphID:      targetGraphID,
		ElementID:    "element1",
		PropertyName: "property1",
	}
	request := &domain.SignedTransaction{
		SenderBlockchainAddress: "address1",
	}

	// Execute
	_, err := useCase.DecodeElement(targetConfig, request)

	// Assert
	if err == nil {
		t.Error("Expected error, got nil")
	}

	assert.Error(t, err)

	blackHole.AssertExpectations(t)
	store.AssertExpectations(t)
}

func TestDecodeElementWhenElementNotFound(t *testing.T) {
	targetGraphID := uuid.New()
	// Setup
	store := keystore.NewStoreMock(t)
	blackHole := blackhole.NewStoreMock(t)
	blackHole.On("LoadGraph", targetGraphID).Return(&graph.Graph{
		Root: graph.NewGraphElement("root"),
	}, nil)
	useCase := newDecodeElementUseCase(store, blackHole)

	targetConfig := TargetElementConfig{
		GraphID:      targetGraphID,
		ElementID:    "element1",
		PropertyName: "property1",
	}
	request := &domain.SignedTransaction{
		SenderBlockchainAddress: "address1",
	}

	// Execute
	_, err := useCase.DecodeElement(targetConfig, request)

	// Assert
	if err == nil {
		t.Error("Expected error, got nil")
	}

	assert.Error(t, err)

	store.AssertExpectations(t)
	blackHole.AssertExpectations(t)
}

func TestDecodeElementWhenAccessDenied(t *testing.T) {
	targetGraphID := uuid.New()
	// Setup
	store := keystore.NewStoreMock(t)
	blackHole := blackhole.NewStoreMock(t)
	useCase := newDecodeElementUseCase(store, blackHole)
	g := &graph.Graph{
		Root: &graph.Element{
			ID: "element1",
			Properties: map[string]string{
				"property1.access_control": `{"access_mode": "address_list", "authorized_addresses": ["differentAddress"]}`,
			},
		},
	}
	blackHole.On("LoadGraph", targetGraphID).Return(g, nil)

	targetConfig := TargetElementConfig{
		GraphID:      targetGraphID,
		ElementID:    "element1",
		PropertyName: "property1",
	}
	request := &domain.SignedTransaction{
		SenderBlockchainAddress: "address1",
	}

	// Execute
	_, err := useCase.DecodeElement(targetConfig, request)

	// Assert
	if err == nil {
		t.Error("Expected error, got nil")
	}

	assert.Error(t, err)

	store.AssertExpectations(t)
	blackHole.AssertExpectations(t)
}

func TestTestDecodeElementWhenDecryptionFails(t *testing.T) {
	targetGraphID := uuid.New()
	// Setup
	store := keystore.NewStoreMock(t)
	blackHole := blackhole.NewStoreMock(t)

	useCase := newDecodeElementUseCase(store, blackHole)
	g := &graph.Graph{
		Root: &graph.Element{
			ID: "element1",
			Properties: map[string]string{
				"property1.access_control": `{"access_mode": "owner"}`,
				"property1.encoded":        "encodedValue",
			},
		},
	}
	blackHole.On("LoadGraph", targetGraphID).Return(g, nil)

	targetConfig := TargetElementConfig{
		GraphID:      targetGraphID,
		ElementID:    "element1",
		PropertyName: "property1",
	}
	request := &domain.SignedTransaction{
		SenderBlockchainAddress: "address1",
	}

	// Execute
	_, err := useCase.DecodeElement(targetConfig, request)

	// Assert
	if err == nil {
		t.Error("Expected error, got nil")
	}
}
