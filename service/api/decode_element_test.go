package api

import (
	"bytes"
	"encoding/json"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
	"utopixia.com/paradox/domain"
	"utopixia.com/paradox/usecase"
)

func TestDecodeElementHandlerHappyPath(t *testing.T) {
	mockUseCase := usecase.NewMockDecodeElementUseCases(t)
	uc := usecase.UseCases{
		DecodeElementUseCase: mockUseCase,
	}
	wallet := domain.NewWallet()
	userAddress := wallet.BlockchainAddress()
	elementID := "element1"
	targetGraphID := uuid.New()
	expectedElementConfig := usecase.TargetElementConfig{
		GraphID:      targetGraphID,
		ElementID:    elementID,
		PropertyName: "property1",
	}

	request := ElementConfig{
		GraphID:      targetGraphID.String(),
		ElementID:    elementID,
		PropertyName: "property1",
		Action:       "decode_element",
	}

	requestBytes, _ := json.Marshal(request)
	requestTx := domain.Transaction{
		UUID:                       uuid.New(),
		SenderPublicKey:            wallet.PublicKey(),
		SenderBlockchainAddress:    userAddress,
		RecipientBlockchainAddress: userAddress,
		Value:                      0,
		Data:                       string(requestBytes),
	}

	signedTx := requestTx.Sign(wallet.PrivateKey())
	requestTxBytes, _ := json.Marshal(signedTx)

	mockUseCase.On("DecodeElement", expectedElementConfig, signedTx).Return("test", nil)

	handler := NewDecodeElementHandler(wallet, uc)

	req, _ := http.NewRequest("POST", "/decode_element", bytes.NewBuffer(requestTxBytes))
	rr := httptest.NewRecorder()

	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	mockUseCase.AssertExpectations(t)
}

func TestDecodeElementHandlerInvalidAction(t *testing.T) {
	mockUseCase := usecase.NewMockDecodeElementUseCases(t)
	uc := usecase.UseCases{
		DecodeElementUseCase: mockUseCase,
	}
	wallet := domain.NewWallet()
	userAddress := wallet.BlockchainAddress()
	elementID := "element1"
	targetGraphID := uuid.New()

	request := ElementConfig{
		GraphID:      targetGraphID.String(),
		ElementID:    elementID,
		PropertyName: "property1",
		Action:       "invalid_action",
	}

	requestBytes, _ := json.Marshal(request)
	requestTx := domain.Transaction{
		UUID:                       uuid.New(),
		SenderPublicKey:            wallet.PublicKey(),
		SenderBlockchainAddress:    userAddress,
		RecipientBlockchainAddress: userAddress,
		Value:                      0,
		Data:                       string(requestBytes),
	}

	signedTx := requestTx.Sign(wallet.PrivateKey())
	requestTxBytes, _ := json.Marshal(signedTx)

	handler := NewDecodeElementHandler(wallet, uc)

	req, _ := http.NewRequest("POST", "/decode_element", bytes.NewBuffer(requestTxBytes))
	rr := httptest.NewRecorder()

	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	mockUseCase.AssertExpectations(t)
}

func TestDecodeElementHandlerUseCaseError(t *testing.T) {
	mockUseCase := usecase.NewMockDecodeElementUseCases(t)
	uc := usecase.UseCases{
		DecodeElementUseCase: mockUseCase,
	}
	wallet := domain.NewWallet()
	userAddress := wallet.BlockchainAddress()
	elementID := "element1"
	targetGraphID := uuid.New()
	expectedElementConfig := usecase.TargetElementConfig{
		GraphID:      targetGraphID,
		ElementID:    elementID,
		PropertyName: "property1",
	}

	request := ElementConfig{
		GraphID:      targetGraphID.String(),
		ElementID:    elementID,
		PropertyName: "property1",
		Action:       "decode_element",
	}

	requestBytes, _ := json.Marshal(request)
	requestTx := domain.Transaction{
		UUID:                       uuid.New(),
		SenderPublicKey:            wallet.PublicKey(),
		SenderBlockchainAddress:    userAddress,
		RecipientBlockchainAddress: userAddress,
		Value:                      0,
		Data:                       string(requestBytes),
	}

	signedTx := requestTx.Sign(wallet.PrivateKey())
	requestTxBytes, _ := json.Marshal(signedTx)

	mockUseCase.On("DecodeElement", expectedElementConfig, signedTx).Return("test", assert.AnError)

	handler := NewDecodeElementHandler(wallet, uc)
	req, _ := http.NewRequest("POST", "/decode_element", bytes.NewBuffer(requestTxBytes))
	rr := httptest.NewRecorder()

	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	mockUseCase.AssertExpectations(t)
}
