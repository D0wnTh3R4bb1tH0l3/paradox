package keystore_test

import (
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"testing"
	"utopixia.com/paradox/infrastructure/keystore"
)

func TestKeyGenerationAndRetrieval(t *testing.T) {
	userOne := uuid.New()

	store := keystore.NewInMemoryStore()
	err := store.GenerateKey(userOne, "owner")
	assert.Nil(t, err)

	key, err := store.GetKey(userOne)
	assert.NotNil(t, key)
	assert.Nil(t, err)

}

func TestKeyGenerationDuplication(t *testing.T) {
	userOne := uuid.New()

	store := keystore.NewInMemoryStore()
	err := store.GenerateKey(userOne, "owner")
	assert.Nil(t, err)

	err = store.GenerateKey(userOne, "owner")
	assert.NotNil(t, err)
}

func TestKeyRemoval(t *testing.T) {
	userOne := uuid.New()

	store := keystore.NewInMemoryStore()
	err := store.GenerateKey(userOne, "owner")
	assert.Nil(t, err)

	err = store.RemoveKey(userOne, "owner")
	assert.Nil(t, err)
}

func TestKeyRemovalNonExistent(t *testing.T) {
	userOne := uuid.New()

	store := keystore.NewInMemoryStore()
	err := store.RemoveKey(userOne, "owner")
	assert.NotNil(t, err)
}

func TestKeyRetrievalNonExistent(t *testing.T) {
	userOne := uuid.New()

	store := keystore.NewInMemoryStore()
	key, err := store.GetKey(userOne)
	assert.Nil(t, key)
	assert.NotNil(t, err)
}
