package keystore

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
)

type RSAKeyWrapper struct {
	privateKey   *rsa.PrivateKey
	ownerAddress string
}

func NewRSAKeyWrapper(privateKey *rsa.PrivateKey, ownerAddress string) *RSAKeyWrapper {
	return &RSAKeyWrapper{
		privateKey:   privateKey,
		ownerAddress: ownerAddress,
	}
}

// Encrypt the data with the public key and return the encrypted data
func (k *RSAKeyWrapper) Encrypt(data []byte) ([]byte, error) {
	return rsa.EncryptPKCS1v15(rand.Reader, &k.privateKey.PublicKey, data)
}

// Decrypt the data with the private key and return the decrypted data
func (k *RSAKeyWrapper) Decrypt(data []byte) ([]byte, error) {
	return rsa.DecryptPKCS1v15(rand.Reader, k.privateKey, data)
}

// Encode the private key to PEM format in a byte array
func (k *RSAKeyWrapper) Encode() ([]byte, error) {
	encoded := x509.MarshalPKCS1PublicKey(&k.privateKey.PublicKey)
	return encoded, nil
}

// Owner return the owner address of the key
func (k *RSAKeyWrapper) Owner() string {
	return k.ownerAddress
}
