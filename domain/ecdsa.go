package domain

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"encoding/hex"
	"fmt"
	"golang.org/x/crypto/sha3"
	"math/big"
)

type Signature struct {
	R *big.Int
	S *big.Int
}

func (s *Signature) IsValidFor(publicKey *ecdsa.PublicKey, message []byte) bool {
	h := sha3.Sum256(message)
	return ecdsa.Verify(publicKey, h[:], s.R, s.S)
}

func (s *Signature) String() string {
	return fmt.Sprintf("%064x%064x", s.R, s.S)
}

func StringToBigIntTuple(s string) (big.Int, big.Int) {
	bx, _ := hex.DecodeString(s[:64])
	by, _ := hex.DecodeString(s[64:])

	var bigx big.Int
	var bigy big.Int

	_ = bigx.SetBytes(bx)
	_ = bigy.SetBytes(by)
	return bigx, bigy
}

func SignatureFromString(s string) *Signature {
	x, y := StringToBigIntTuple(s)
	return &Signature{&x, &y}
}
func PublicKeyFromString(s string) *ecdsa.PublicKey {
	x, y := StringToBigIntTuple(s)
	return &ecdsa.PublicKey{Curve: elliptic.P256(), X: &x, Y: &y}
}

func PrivateKeyFromString(s string, publicKey *ecdsa.PublicKey) *ecdsa.PrivateKey {
	b, _ := hex.DecodeString(s[:])
	var bi big.Int
	_ = bi.SetBytes(b)
	return &ecdsa.PrivateKey{*publicKey, &bi}
}

func PrivateKeyToString(privateKey *ecdsa.PrivateKey) string {
	return fmt.Sprintf("%x", privateKey.D.Bytes())
}

func PublicKeyToString(publicKey *ecdsa.PublicKey) string {
	return fmt.Sprintf("%064x%064x", publicKey.X.Bytes(), publicKey.Y.Bytes())
}
