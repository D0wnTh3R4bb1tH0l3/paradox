package api

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"github.com/google/uuid"
	"net/http"
	"utopixia.com/paradox/domain"
	"utopixia.com/paradox/usecase"
)

func NewGetKeyHandler(wallet *domain.Wallet, uc usecase.UseCases) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var signedTransaction domain.SignedTransaction
		// Parse the request
		err := json.NewDecoder(r.Body).Decode(&signedTransaction)
		if err != nil {
			NewSignedResponseWriter(wallet, w).SendSignedJSON(http.StatusBadRequest, JSONResponse{
				Success: false,
				Error:   err.Error(),
			})
			return
		}

		// Verify the transaction
		if err = signedTransaction.IsValid(); err != nil {
			NewSignedResponseWriter(wallet, w).SendSignedJSON(http.StatusBadRequest, JSONResponse{
				Success: false,
				Error:   err.Error(),
			})
			return
		}

		// Parse the request
		var request domain.KeyAccessRequest
		err = json.Unmarshal([]byte(signedTransaction.Data), &request)
		if err != nil {
			NewSignedResponseWriter(wallet, w).SendSignedJSON(http.StatusBadRequest, JSONResponse{
				Success: false,
				Error:   err.Error(),
			})
			return
		}

		// Verify action
		if request.Action != "get_key" {
			NewSignedResponseWriter(wallet, w).SendSignedJSON(http.StatusBadRequest, JSONResponse{
				Success: false,
				Error:   "invalid action type",
			})
			return
		}

		parsedKeyID, err := uuid.Parse(request.KeyID)
		if err != nil {
			NewSignedResponseWriter(wallet, w).SendSignedJSON(http.StatusBadRequest, JSONResponse{
				Success: false,
				Error:   err.Error(),
			})
			return
		}

		// Get the key
		key, err := uc.GetKeyUseCase.GetKey(parsedKeyID)
		if err != nil {
			if errors.Is(err, usecase.ErrKeyNotFound) {
				NewSignedResponseWriter(wallet, w).SendSignedJSON(http.StatusNotFound, JSONResponse{
					Success: false,
					Error:   err.Error(),
				})
				return
			}
			NewSignedResponseWriter(wallet, w).SendSignedJSON(http.StatusBadRequest, JSONResponse{
				Success: false,
				Error:   err.Error(),
			})
			return
		}

		// Encode key as PEM format
		encodedKey, err := key.Encode()
		if err != nil {
			NewSignedResponseWriter(wallet, w).SendSignedJSON(http.StatusBadRequest, JSONResponse{
				Success: false,
				Error:   err.Error(),
			})
			return
		}

		b64Key := base64.URLEncoding.EncodeToString(encodedKey)

		// Send the key
		NewSignedResponseWriter(wallet, w).SendSignedJSON(http.StatusOK, JSONResponse{
			Success: true,
			Message: b64Key,
		})
	}
}
