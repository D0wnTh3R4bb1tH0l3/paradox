package api

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"github.com/go-chi/chi"
	"utopixia.com/paradox/domain"
	"utopixia.com/paradox/usecase"
)

func NewRouter(wallet *domain.Wallet, uc usecase.UseCases) *chi.Mux {
	router := BuildChiRouter(wallet)
	router.Post("/rsa/generate", NewGenerateKeyHandler(wallet, uc))
	router.Post("/rsa/revoke", NewRevokeKeyHandler(wallet, uc))
	router.Post("/rsa/get", NewGetKeyHandler(wallet, uc))
	router.Post("/rsa/graph/decode", NewDecodeElementHandler(wallet, uc))
	return router
}
