package keystore

import "github.com/google/uuid"

/*
 * This file is part of Paradox
 * Copyright (C) 2024 Ororea Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// KeyType Enum for key types
type KeyType int

const (
	RSA KeyType = iota
)

type KeyWrapper interface {
	Encrypt([]byte) ([]byte, error)
	Decrypt([]byte) ([]byte, error)
	Encode() ([]byte, error)
	Owner() string
}

type Store interface {
	// GetKey a key from the store
	GetKey(keyID uuid.UUID) (KeyWrapper, error)
	// GenerateKey a key in the store
	GenerateKey(keyID uuid.UUID, ownerAddress string) error
	// RemoveKey a key from the store
	RemoveKey(keyID uuid.UUID, requestSenderAddress string) error
	// PutKey a key in the store
	PutKey(keyID uuid.UUID, key KeyWrapper) error
}
