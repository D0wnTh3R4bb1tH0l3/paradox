# Paradox - Gestionnaire de clés pour Utopixia

![build status](https://gitlab.com/D0wnTh3R4bb1tH0l3/paradox/badges/main/pipeline.svg)

Paradox est une application destinée à la gestion sécurisée de clés cryptographiques au sein de l'écosystème Utopixia. Ce service permet de générer, révoquer et récupérer des clés RSA, ainsi que de décoder des graphes, en s'appuyant sur une architecture robuste et sécurisée.

## Prérequis

Pour utiliser Paradox, vous devez avoir Docker installé sur votre machine. Pour installer Docker, veuillez suivre les instructions sur le site officiel de Docker à [docker.com](https://www.docker.com/get-started).

## Configuration initiale

Avant de démarrer l'application, vous devez générer un fichier `wallet.webcode`, qui contiendra vos informations de portefeuille sécurisé. Ce fichier est essentiel pour l'authentification et la gestion des clés au sein de l'application.

## Déploiement avec Docker

Le projet inclut un `Dockerfile` pour faciliter le déploiement de l'application. Voici les étapes à suivre pour construire et exécuter le conteneur Docker :

1. **Construire l'image Docker** :
    - Ouvrez un terminal.
    - Naviguez jusqu'au dossier contenant le `Dockerfile`.
    - Exécutez la commande suivante pour construire l'image Docker :
      ```
      docker build -t paradox .
      ```

2. **Exécuter le conteneur Docker** :
    - Assurez-vous que le fichier `wallet.webcode` est dans votre répertoire courant.
    - Lancez l'application en utilisant la commande suivante :
      ```
      docker run -p 80:80 -v $(pwd)/wallet.webcode:/app/wallet.webcode paradox
      ```

## Utilisation

Une fois le conteneur en cours d'exécution, Paradox est prêt à être utilisé à travers les routes API exposées. Vous pouvez interagir avec l'API via des requêtes HTTP envoyées aux endpoints définis pour les opérations de gestion des clés.

## Contribution

Les contributions à Paradox sont les bienvenues. Pour contribuer au projet, veuillez cloner ce dépôt, créer une nouvelle branche pour vos modifications et soumettre une pull request une fois vos modifications complétées.

## Licence

Paradox est publié sous la licence GPL. Pour plus d'informations, voir le fichier `LICENSE.md` dans ce dépôt.
