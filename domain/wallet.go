package domain

/*
 * This file is part of Paradox (or Your Project Name)
 * Copyright (C) [Year] [Your Name or Your Organization's Name]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/btcsuite/btcutil/base58"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"golang.org/x/crypto/ripemd160"
	"golang.org/x/crypto/sha3"
	"math/big"
	"strings"
	"utopixia.com/paradox/infrastructure/ecies"
	"utopixia.com/paradox/infrastructure/logging"
)

type Wallet struct {
	privateKey        *ecdsa.PrivateKey
	publicKey         *ecdsa.PublicKey
	domain            string
	blockchainAddress string
	Children          []*Wallet
}

func New(privateKey string, publicKey string, address string) (*Wallet, error) {
	public := PublicKeyFromString(publicKey)
	if public == nil {
		return nil, errors.New("invalid public key")

	}
	private := PrivateKeyFromString(privateKey, public)
	if private == nil {
		return nil, errors.New("invalid private key")
	}
	return &Wallet{
		privateKey:        private,
		publicKey:         public,
		blockchainAddress: address,
	}, nil
}

func NewWallet() *Wallet {
	return NewWalletWithDomain("utopixia.com")
}

func NewWalletWithDomain(domain string) *Wallet {
	w := new(Wallet)
	privateKey, _ := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	w.privateKey = privateKey
	w.publicKey = &w.privateKey.PublicKey
	w.blockchainAddress = AddressFromPublicKey(w.publicKey)
	w.Children = []*Wallet{}
	w.domain = domain
	return w
}

func AddressFromPublicKey(publicKey *ecdsa.PublicKey) string {
	h2 := sha256.New()
	h2.Write(publicKey.X.Bytes())
	h2.Write(publicKey.Y.Bytes())
	digest2 := h2.Sum(nil)

	h3 := ripemd160.New()
	h3.Write(digest2)
	digest3 := h3.Sum(nil)

	vd4 := make([]byte, 21)
	vd4[0] = 0x00
	copy(vd4[1:], digest3[:])
	h5 := sha256.New()
	h5.Write(vd4)
	digest5 := h5.Sum(nil)

	h6 := sha256.New()
	h6.Write(digest5)
	digest6 := h6.Sum(nil)

	chsum := digest6[:4]
	dc8 := make([]byte, 25)
	copy(dc8[:21], vd4[:])
	copy(dc8[21:], chsum[:])
	address := base58.Encode(dc8)
	return address
}

func (w *Wallet) ToWebcode(domain string) string {
	if domain == "" && w.domain == "" {
		return ""
	}
	j, err := jwk.FromRaw(w.PrivateKey())
	if err != nil {
		logging.Error("can't get jwk : %s", err)
		return ""
	}
	err = j.Set("address", w.BlockchainAddress())
	err = j.Set("domain", w.domain)
	if domain != "" {
		err = j.Set("domain", domain)
	}
	err = j.Set("public_key", w.PublicKeyStr())
	if err != nil {
		logging.Error("can't set address : %s", err)
		return ""
	}
	buf, err := json.MarshalIndent(j, "", "  ")
	if err != nil {
		logging.Error("failed to marshal key into JSON: %s\n", err)
		return ""
	}

	return fmt.Sprintf("%s\n", base64.RawStdEncoding.EncodeToString(buf))
}

// adjustToMultipleOfFour adjusts a string so that its length is a multiple of 4.
// If necessary, it appends equal signs (=) to the end of the string.
func adjustToMultipleOfFour(s string) string {
	s = strings.TrimSpace(s)
	remainder := len(s) % 4
	if remainder == 0 {
		return s
	}

	// Ajouter des signes égal en fonction du reste
	numOfEquals := 4 - remainder
	for i := 0; i < numOfEquals; i++ {
		s += "="
	}
	return s
}

func FromWebcode(webcode string) (*Wallet, error) {
	decodedWebcode, err := base64.URLEncoding.DecodeString(adjustToMultipleOfFour(webcode))
	if err != nil {
		return nil, err
	}
	// Parse le JWK
	var keyData map[string]string
	err = json.Unmarshal(decodedWebcode, &keyData)
	if err != nil {
		return nil, err
	}

	// Récupère les composants de clé
	x := keyData["x"]
	y := keyData["y"]
	d := keyData["d"]

	// Convertit les composants en octets
	xBytes, _ := decodeBase64URL(x)
	yBytes, _ := decodeBase64URL(y)
	dBytes, _ := decodeBase64URL(d)

	// Crée une structure de clé publique ECDSA
	publicKey := &ecdsa.PublicKey{
		Curve: elliptic.P256(),
		X:     new(big.Int).SetBytes(xBytes),
		Y:     new(big.Int).SetBytes(yBytes),
	}

	// Crée une structure de clé privée ECDSA
	privateKey := &ecdsa.PrivateKey{
		PublicKey: *publicKey,
		D:         new(big.Int).SetBytes(dBytes),
	}

	w := NewWallet()
	w.publicKey = publicKey
	w.privateKey = privateKey
	w.blockchainAddress = keyData["address"]
	w.domain = keyData["domain"]
	return w, nil
}

func (w *Wallet) Sign(data []byte) (*Signature, error) {
	h := sha3.Sum256(data)
	r, s, err := ecdsa.Sign(rand.Reader, w.privateKey, h[:])
	if err != nil {
		return nil, err
	}
	return &Signature{R: r, S: s}, nil
}

func (w *Wallet) PrivateKey() *ecdsa.PrivateKey {
	return w.privateKey
}

func (w *Wallet) PrivateKeyStr() string {
	return fmt.Sprintf("%x", w.privateKey.D.Bytes())
}

func (w *Wallet) Domain() string {
	return w.domain
}

func (w *Wallet) PublicKey() *ecdsa.PublicKey {
	return w.publicKey
}

func (w *Wallet) PublicKeyStr() string {
	return fmt.Sprintf("%064x%064x", w.publicKey.X.Bytes(), w.publicKey.Y.Bytes())
}

func (w *Wallet) BlockchainAddress() string {
	return w.blockchainAddress
}

func (w *Wallet) UnmarshalJSON(data []byte) error {
	v := &struct {
		PrivateKey        string `json:"private_key"`
		PublicKey         string `json:"public_key"`
		BlockchainAddress string `json:"blockchain_address"`
	}{}

	err := json.Unmarshal(data, v)
	if err != nil {
		return err
	}
	publicKey := PublicKeyFromString(v.PublicKey)
	privateKey := PrivateKeyFromString(v.PrivateKey, publicKey)

	w.privateKey = privateKey
	w.publicKey = publicKey
	w.blockchainAddress = v.BlockchainAddress
	return nil
}

func (w *Wallet) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		PrivateKey        string `json:"private_key"`
		PublicKey         string `json:"public_key"`
		BlockchainAddress string `json:"blockchain_address"`
	}{
		PrivateKey:        w.PrivateKeyStr(),
		PublicKey:         w.PublicKeyStr(),
		BlockchainAddress: w.BlockchainAddress(),
	})
}

type EncryptedPayload struct {
	Encrypted bool   `json:"encrypted"`
	S1        string `json:"s1"`
	S2        string `json:"s2"`
	M         string `json:"sm"`
}

func (e *EncryptedPayload) Decrypt(w Wallet) string {
	private := ecies.ImportECDSA(w.privateKey)
	decrypted, err := private.Decrypt([]byte(e.M), []byte(e.S1), []byte(e.S2))
	if err != nil {
		return ""
	}
	return string(decrypted)
}

func (e *EncryptedPayload) ToString() string {
	m, err := json.Marshal(e)
	if err != nil {
		return ""
	}
	return base64.RawStdEncoding.EncodeToString(m)
}

func (w *Wallet) Encrypt(message string) *EncryptedPayload {
	payload := EncryptedPayload{
		Encrypted: true,
		S1:        "S1",
		S2:        "S2",
		M:         "",
	}

	private := ecies.ImportECDSA(w.privateKey)
	encrypted, err := ecies.Encrypt(rand.Reader, &private.PublicKey, []byte(message), []byte(payload.S1), []byte(payload.S2))
	if err != nil {
		return nil
	}

	payload.M = string(encrypted)
	return &payload
}

// Décode une chaîne base64url en octets
func decodeBase64URL(input string) ([]byte, error) {
	// Remplace les caractères spéciaux base64url
	input = input + "==="
	input = strings.ReplaceAll(input, "-", "+")
	input = strings.ReplaceAll(input, "_", "/")

	// Décode en base64 standard
	return base64.StdEncoding.DecodeString(input)
}
