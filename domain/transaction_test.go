package domain_test

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"math/big"
	"testing"
	"utopixia.com/paradox/domain"
)

func generateKeyPair() (*ecdsa.PrivateKey, *ecdsa.PublicKey) {
	privateKey, _ := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	return privateKey, &privateKey.PublicKey
}

func TestNewTransaction(t *testing.T) {
	_, publicKey := generateKeyPair()
	transaction := domain.NewTransaction(publicKey, uuid.New(), "sender", "recipient", 10, "Data")

	assert.NotNil(t, transaction)
	assert.Equal(t, "sender", transaction.SenderBlockchainAddress)
	assert.Equal(t, "recipient", transaction.RecipientBlockchainAddress)
	assert.Equal(t, int64(10), transaction.Value)
	assert.Equal(t, "Data", transaction.Data)
}

func TestGenerateSignature(t *testing.T) {
	privateKey, publicKey := generateKeyPair()
	transaction := domain.NewTransaction(publicKey, uuid.New(), "sender", "recipient", 10, "Data")

	signature := transaction.GenerateSignature(privateKey)

	assert.NotNil(t, signature)
}

func TestIsValid(t *testing.T) {
	wallet := domain.NewWallet()
	transaction := domain.NewTransaction(wallet.PublicKey(), uuid.New(), wallet.BlockchainAddress(), "recipient", 10, "Data")
	signedTransaction := transaction.Sign(wallet.PrivateKey())

	assert.NoError(t, signedTransaction.IsValid())
}

func TestIsValidWithInvalidSignature(t *testing.T) {
	t.Run("invalid_sender", func(t *testing.T) {
		privateKey, publicKey := generateKeyPair()
		transaction := domain.NewTransaction(publicKey, uuid.New(), "sender", "recipient", 10, "Data")

		invalidSignature := &domain.Signature{R: big.NewInt(0), S: big.NewInt(0)}

		signedTransaction := transaction.Sign(privateKey)
		signedTransaction.Signature = invalidSignature.String()

		assert.Error(t, signedTransaction.IsValid())
	})

	t.Run("invalid_signature", func(t *testing.T) {
		wallet := domain.NewWallet()
		transaction := domain.NewTransaction(wallet.PublicKey(), uuid.New(), wallet.BlockchainAddress(), "recipient", 10, "Data")

		invalidSignature := &domain.Signature{R: big.NewInt(0), S: big.NewInt(0)}

		signedTransaction := transaction.Sign(wallet.PrivateKey())
		signedTransaction.Signature = invalidSignature.String()

		assert.Error(t, signedTransaction.IsValid())
	})
}
